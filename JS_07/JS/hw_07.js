// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//     Технические требования:
//     Создать функцию, которая будет принимать на вход массив.
//     Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
// Необязательное задание продвинутой сложности:

   // Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
    // Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.



let div=document.createElement('div');
document.body.append(div);
let text = document.createElement('div');
document.body.append(text);
let counter =10;
function timeCounter(){
  counter=counter-1
}

text.style.fontSize ='45px';
text.style.border ='1px solid grey';
text.style.display ='inline-block';
text.style.backgroundColor ='lightpink';

let id =setInterval(function () {
    timeCounter();
    text.innerHTML=`Information will be delete in ${counter} second`;
    if (counter===0) {
        document.body.innerHTML="";
        clearInterval(id)
    }
},1000);


let myArr =['Kiev', ['City', 'Town'], 'Kharkiv', 'Odessa', 'Lviv'];


function renderList(arr) {
   return  div.innerHTML +=`<ul>${arr.map(item=> Array.isArray(item)? item.map(item=>`<ul><li>${item}</li></ul>`).join(''):`<li>${item}</li>`).join('')}</ul>`
}
renderList(myArr);


