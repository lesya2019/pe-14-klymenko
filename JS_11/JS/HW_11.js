//Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана
// эта буква, должна окрашиваться в синий цвет. При этом, если какая-то
// другая буква уже ранее была окрашена в синий цвет - она становится
// черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет.
// Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет,
// а кнопка Enter опять становится черной.

 document.addEventListener('keypress', function (event) {
    const keyPres =event.key;

    let btnColor = document.getElementsByClassName('btn');

    for(let i=0; i< btnColor.length; i++){
        btnColor[i].className ='btn';
        if(btnColor[i].innerHTML.toLowerCase() === keyPres.toLowerCase()){
            btnColor[i].className ='btn active';
        }
    }
});