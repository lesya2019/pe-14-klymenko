
// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку
// отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт.
// В комментариях указано, какой текст должен отображаться для какой вкладки.
//Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//Нужно предусмотреть, что текст на вкладках может меняться,
// и что вкладки могут добавляться и удаляться. При этом нужно,
// чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.



    // let elems = document.getElementsByClassName('tabs-title');

    const menu = document.getElementsByClassName('tabs')[0];

    menu.addEventListener('click', function (event) {
        const menuItems = event.currentTarget.children;
        console.log(menuItems);
        for (let item of menuItems) {
            item.classList.remove('active');
        }
        event.target.classList.add('active');

        const contentItems = document.getElementsByClassName('tabs-content');
        for (let item of contentItems) {
            item.classList.remove('active');
        }
        console.log(event.target.dataset.id);
        const targetContent = document.getElementById(event.target.dataset.id);
        targetContent.classList.add('active');

    });

