const menu = document.getElementsByClassName('tabs')[0];

menu.addEventListener('click', function (event) {
    const menuItems = event.currentTarget.children;
    console.log(menuItems);
    for (let item of menuItems) {
        item.classList.remove('active');
    }
    event.target.classList.add('active');

    const contentItems = document.getElementsByClassName('text_ourServices_wrapp');
    for (let item of contentItems) {
        item.classList.remove('active');
        item.classList.add('inactive');
    }
    console.log(event.target.dataset.id);
    const targetContent = document.getElementById(event.target.dataset.id);
    targetContent.classList.add('active');

});

const tabContainer = document.querySelector('.list_of_amazing_works');
tabContainer.addEventListener('click', function (e) {
    console.log(e.target.className);
    let images = amazing_images.getElementsByClassName('all');
    console.log(images);

    for (let i = 0; i < images.length; i++) {
        console.dir(e.target.getAttribute('id'));
        if (!images[i].classList.contains(e.target.getAttribute('id'))) {
            images[i].style.display = 'none'
        } else {
            images[i].style.display = 'block'
        }
    }
});

function newVisible(event) {
    document.querySelector('.visible_review').classList.remove('visible_review');
    document.querySelector('.active_item').classList.remove('active_item');

    const newLi = event.target;
    newLi.classList.add('active_item');
    const newLiClass = event.target.classList[1];
    const review = document.querySelectorAll('.' + newLiClass)[1];
    console.log(review);
    review.classList.add('visible_review');
}


let itemLi = document.getElementsByClassName('item');
for (let i = 0; i < itemLi.length; i++) {
    itemLi[i].addEventListener('click', newVisible);
}
const prevArrow = document.querySelector('.previous');
prevArrow.addEventListener('click', sliderLeft);
let left = 0;

function sliderLeft() {
    const move = document.querySelector('.carousel_ul');
    left = left - 96;
    if (left < -384) {
        left = 0;
    }
    move.style.left = left + 'px';
}


const nextArrow = document.querySelector('.next');
nextArrow.addEventListener('click', sliderRight);
let right = 0;

function sliderRight() {
    const move = document.querySelector('.carousel_ul');
    right = right + 96;
    if (right > 384) {
        right = 0;
    }
    move.style.right = right + 'px';
}



