const name = prompt('What is your name');
const surname = prompt('What is your surname');
const dateString = prompt('Enter birthday (dd.mm.yyyy)');

function getAge(birthDate){
    let today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())){
        age--;
    }
    return age;
}

function createNewUser(name, surname, birthDate) {
    const newUser = {};
    newUser.getAge = getAge(birthDate);

    Object.defineProperty(newUser, 'firstName', {
        enumerable: true,
        configurable: true,
        value: name
    });

    Object.defineProperty(newUser, 'lastName', {
        enumerable: true,
        configurable: true,
        value: surname
    });

    Object.defineProperty(newUser, 'age', {
        enumerable: true,
        configurable: true,
        value: birthDate.getFullYear()
    });

    Object.defineProperty(newUser, 'getPassword', {
        value: function () {
            return `${
                this.firstName
                    .charAt(0).toUpperCase()}${
                this.lastName
                    .toLowerCase()}${
                this.age
                }`;
        }
    });
    return newUser;
}


const birthDate = new Date( +dateString.substring(6,10), +dateString.substring(3,5)-1, +dateString.substring(0,2));

const student = createNewUser(name, surname, birthDate);


console.log(student.getPassword());
console.log(student);
