// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
// Это поле будет служить для ввода числовых значений

// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
// При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span,
// в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
// Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
// под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
window.onload = function () {
    let butt = document.getElementsByClassName('close')[0];
    let input = document.getElementsByClassName('inputPrice')[0];
    let error = document.getElementsByClassName('error')[0];
    let text = document.getElementsByClassName('text')[0];
    let current = document.getElementsByClassName('currPrice')[0];


    input.addEventListener('focusout', function () {
        let price = input.value;
        if (price === '' || price < 0) {
            input.style.border = '2px solid darkred';
            error.style.display = 'block';
        } else {
            input.style.border = '2px solid green';
            text.innerText = `Текущая цена: ${price}$`;
            error.style.display = 'none';
            current.style.display = 'block';
        }
    });
    butt.addEventListener('click', function () {
        current.style.display = 'none';
        input.style.borderColor = 'transparent';
        input.value = '';
    });
};